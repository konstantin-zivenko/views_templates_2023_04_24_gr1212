from django.urls import path
from polls import views, views_gen


app_name = "polls"
# urlpatterns = [
#     path('', views.index, name="index"),
#     path('<int:question_id>/', views.detail, name='detail'),
#     path('<int:question_id>/results/', views.results, name='results'),
#     path('<int:question_id>/vote/', views.vote, name='vote'),
# ]

urlpatterns = [
    path('', views_gen.IndexView.as_view(), name="index"),
    path('<int:pk>/', views_gen.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views_gen.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
    path('api/v1/questions/', views.question_collections),
    path('api/v1/questions/<int:id>', views.question)
]
