# views_templates_2023_04_24_gr1212

---

[video - 1](https://youtu.be/o3jOJK8hJb0) - create project, app, models, create and apply migrations, superuser and admin

[video - 2](https://youtu.be/vIaEoZpD2ME) - views, template (part 2)

[video - 3](<https://youtu.be/sFakHMMwfEA>) - templates

[video - 4](https://youtu.be/xJZpIKVJfQM) - forms, generic

[video - 5](https://youtu.be/2ryfyqigvtA) - DRF, REST

[video - 6](https://youtu.be/y_3fVm_I7EM) - deploy pythonanywhere

